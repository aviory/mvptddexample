package yurasik.com.smallmvpexample;

import yurasik.com.smallmvpexample.mvp.view.CreatePersonMvpView;

/**
 * Created by avi on 26.06.17.
 */

public class MockView implements CreatePersonMvpView{
    String fname;
    String lname;

    @Override
    public String getFirstName() {
        return fname;
    }

    @Override
    public String getLastName() {
        return lname;
    }

    @Override
    public void setFirstName(String fname) {
        this.fname = fname;
    }

    @Override
    public void setLastName(String lname) {
        this.lname = lname;
    }

    @Override
    public void clearEditTexts() {
        fname = "null";
        lname = "null";
    }
}
