package yurasik.com.smallmvpexample;

import yurasik.com.smallmvpexample.data.DataStorageInterface;
import yurasik.com.smallmvpexample.mvp.model.Person;

/**
 * Created by avi on 26.06.17.
 */

public class MockDB implements DataStorageInterface {
    private Person p;

    @Override
    public void savePerson(Person person) {
        p = person;
    }

    @Override
    public Person readPerson() {
        if(p==null){
            p = new Person();
            p.setFirstName("null");
            p.setLastName("null");
            return p;
        }
        return p;
    }

    @Override
    public void clear() {
        p = null;
    }
}
