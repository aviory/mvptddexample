package yurasik.com.smallmvpexample;

import org.junit.Test;

import yurasik.com.smallmvpexample.data.MockDataStorage;
import yurasik.com.smallmvpexample.mvp.model.Person;
import yurasik.com.smallmvpexample.mvp.presenter.CreatePersonPresenter;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void test_btnSave_to_db() throws Exception {
        MockView mView = new MockView();
        mView.setFirstName(TestConst.PERSON_FIRST_NAME);
        mView.setLastName(TestConst.PERSON_LAST_NAME);

        CreatePersonPresenter mPresenter = new CreatePersonPresenter<>();
        MockDB db =new MockDB();
        mPresenter.attach(mView, db);
        mPresenter.onClickSaveButton(); // click save btn

        assertEquals(db.readPerson().getFirstName(), TestConst.PERSON_FIRST_NAME);//test save to db
        assertEquals(db.readPerson().getLastName(), TestConst.PERSON_LAST_NAME);
    }
    @Test
    public void test_btnRead_from_db() throws Exception {
        MockView mView = new MockView();

        Person p = new Person();
        p.setFirstName(TestConst.PERSON_FIRST_NAME);
        p.setLastName(TestConst.PERSON_LAST_NAME);

        MockDB db =new MockDB();
        db.savePerson(p); // save Person to db

        CreatePersonPresenter mPresenter = new CreatePersonPresenter<>();
        mPresenter.attach(mView, db);


        mPresenter.onClickReadButton(); // click read btn

        assertEquals(mView.getFirstName(), TestConst.PERSON_FIRST_NAME);//test save to db
        assertEquals(mView.getLastName(), TestConst.PERSON_LAST_NAME);
    }
}