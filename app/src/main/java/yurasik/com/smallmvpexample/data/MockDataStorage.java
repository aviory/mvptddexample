package yurasik.com.smallmvpexample.data;

import yurasik.com.smallmvpexample.mvp.model.Person;
import yurasik.com.smallmvpexample.util.LogUtil;

/**
 * Created by Yurii on 6/15/17.
 */

public class MockDataStorage implements DataStorageInterface {
    private Person p;

    @Override
    public void savePerson(Person person) {
        LogUtil.i("Save Person: " + person);
        p = person;
    }

    @Override
    public Person readPerson() {
        if(p==null){
            p = new Person();
            p.setFirstName("null");
            p.setLastName("null");
            return p;
        }
        return p;
    }

    @Override
    public void clear() {
        p = null;
    }
}
