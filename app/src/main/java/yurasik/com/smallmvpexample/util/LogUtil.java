package yurasik.com.smallmvpexample.util;

import android.util.Log;

public class LogUtil {

    public static void i(String message) {
        Log.d("MyLog", message);
    }
}
