package yurasik.com.smallmvpexample.mvp.presenter;

import yurasik.com.smallmvpexample.data.DataStorageInterface;
import yurasik.com.smallmvpexample.mvp.base.BaseMvpView;
import yurasik.com.smallmvpexample.mvp.model.Person;

public interface ICreatePersonPresenter<V extends BaseMvpView> {
    void onClickSaveButton();
    void onClickClearButton();
    void savePerson(Person person);
    Person getPerson();
}
