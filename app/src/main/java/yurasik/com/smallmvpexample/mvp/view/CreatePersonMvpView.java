package yurasik.com.smallmvpexample.mvp.view;

import yurasik.com.smallmvpexample.mvp.base.BaseMvpView;

/**
 * Created by Yurii on 6/15/17.
 */

public interface CreatePersonMvpView extends BaseMvpView{
    String getFirstName();
    String getLastName();
    void setFirstName(String fname);
    void setLastName(String lname);
    void clearEditTexts();
}
