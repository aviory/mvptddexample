package yurasik.com.smallmvpexample.mvp.base;

import android.os.Bundle;

import yurasik.com.smallmvpexample.data.DataStorageInterface;
import yurasik.com.smallmvpexample.util.LogUtil;

public abstract class BasePresenter<V extends BaseMvpView, S extends DataStorageInterface> {

    protected V mView;
    protected S mStorage;

    public void attach(V mView, S mStorage){
        this.mView = mView;
        this.mStorage = mStorage;
//        LogUtil.i("attach: " + mView);
//        LogUtil.i("attach: " + mStorage);
    }

    public void detach(){
//        LogUtil.i("detach: " + mView);
//        LogUtil.i("detach: " + mStorage);
        mView = null;
        mStorage = null;
    }

    public V getMvpView() {
        return mView;
    }
    public S getMvpStorage() {
        return mStorage;
    }

    public abstract void onSave(Bundle state);
    public abstract void onRestote(Bundle state);
}
